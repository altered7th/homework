package worktime;

public class Breaktime extends TimeBase {

	public Breaktime() {
		super();
	}
	public Breaktime( int from, int to ) {
		super( from, to );
	}
	public Breaktime( Breaktime breaktime ) {
		this( breaktime.getFrom(), breaktime.getTo() );
	}
}
