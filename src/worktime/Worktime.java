package worktime;

import java.util.ArrayList;
import java.util.List;

public class Worktime extends TimeBase {

	
	private List<Breaktime> breaktimeList;

	public Worktime() {
		super();
		breaktimeList = new ArrayList<Breaktime>();
	}
	public Worktime( int from, int to ) {
		super( from, to );
		breaktimeList = new ArrayList<Breaktime>();
	}
	
	public void addBreaktime( Breaktime breaktime ) {
		breaktime = new Breaktime( breaktime );
		// Worktime をはみ出た休憩時間を削る
		if ( breaktime.getFrom() < from ) {
			breaktime.setFrom( from );
		}
		if ( breaktime.getTo() > to ) {
			breaktime.setTo( to );
		}
		breaktimeList.add( breaktime );
	}
	public int getWorkMinute() {
		int workMin = getMinute();
		for  ( Breaktime breaktime : breaktimeList ) {
			workMin -= breaktime.getMinute();
		}
		return workMin;
	}
}
