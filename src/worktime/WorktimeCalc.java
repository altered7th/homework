package worktime;

import java.util.ArrayList;
import java.util.List;

public class WorktimeCalc {

	private List<Worktime> worktimeList;
	private List<Breaktime> breaktimeList;
	public WorktimeCalc() {
		worktimeList = new ArrayList<Worktime>();
		breaktimeList = new ArrayList<Breaktime>();
	}
	public void addWorktime( Worktime worktime ) {
		worktimeList.add( worktime );
	}
	public void addBreaktime( Breaktime breaktime ) {
		breaktimeList.add( breaktime );
	}
	public int getTotalWorktime() {
		markBreaktimes();
		int totalMin = 0;
		for ( Worktime worktime : worktimeList ) {
			totalMin += worktime.getWorkMinute();
		}
		return totalMin;
	}
	// Breaktime のマーキングのみ行う
	private void markBreaktimes() {
		for ( Worktime worktime : worktimeList ) {
			for ( Breaktime breaktime : breaktimeList ) {
				int workFrom = worktime.getFrom();
				int workTo = worktime.getTo();
				int breakFrom = breaktime.getFrom();
				int breakTo = breaktime.getTo();
				if ( workFrom < breakFrom && breakFrom < workTo
					|| workFrom < breakTo && breakTo < workTo
					) {
					worktime.addBreaktime( breaktime );
				}
			}
		}
	}
	//---------------------------------------------------------------
	public static void main( String[] args ) {
		WorktimeCalc calc = new WorktimeCalc();
		calc.test2();
	}
	
	public void test2() {
		WorktimeCalc calc = new WorktimeCalc();
		calc.addWorktime( new Worktime( 930, 1800 ) );
		calc.addWorktime( new Worktime( 1900, 2100 ) );

		calc.addBreaktime( new Breaktime( 1200, 1300 ) );
		calc.addBreaktime( new Breaktime( 1400, 1405 ) );
		calc.addBreaktime( new Breaktime( 1700, 2000 ) );
		calc.addBreaktime( new Breaktime( 2045, 2130 ) );
		int total = calc.getTotalWorktime();
		System.out.println( "total=" + total + " HHMM=" + TimeBase.getHHMMString( total ) );
	}
}
