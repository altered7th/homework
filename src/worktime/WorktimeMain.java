package worktime;
public class WorktimeMain {

	public static void main( String[] args ) {
		new WorktimeMain();
	}
	public WorktimeMain() {
		WorktimeCalc calc = new WorktimeCalc();
		calc.addWorktime( new Worktime( 930, 1800 ) );
		calc.addWorktime( new Worktime( 1900, 2200 ) );

		calc.addBreaktime( new Breaktime( 830, 1000 ) );
		calc.addBreaktime( new Breaktime( 1200, 1300 ) );
//		calc.addBreaktime( new Breaktime( 1400, 1405 ) );
		calc.addBreaktime( new Breaktime( 1730, 1930 ) );
		calc.addBreaktime( new Breaktime( 2130, 2230 ) );
		int total = calc.getTotalWorktime();
		System.out.println( "total=" + total + " HHMM=" + TimeBase.getHHMMString( total ) );
	}
}
