package worktime;

public class TimeBase {

	protected int from;
	protected int to;
	
	public TimeBase() {
	}
	public TimeBase( int from, int to ) {
		this();
		this.from = from;
		this.to = to;
	}
	public int getFrom() {
		return from;
	}
	public void setFrom( int from ) {
		this.from = from;
	}
	public int getTo() {
		return to;
	}
	public void setTo( int to ) {
		this.to = to;
	}
	public int getMinute() {
		int fromMin = from / 100 * 60 + from % 100;
		int toMin = to / 100 * 60 + to % 100;
		return toMin - fromMin;
	}
	public String getHHMM() {
		return right( "0" + from, 4 ) + "-" + right( "0" + to, 4 );
	}
	public static int getHHMM( int min ) {
		return min / 60 * 100 + min % 60;
	}
	public static String getHHMMString( int min ) {
		String hhmm = right( "00" + getHHMM( min ), 4 );
		return hhmm.substring( 0, 2 ) + ":" + hhmm.substring( 2, 4 );
	}
	
	public static String right( String s, int len ) {
		if ( s.length() < len ) {
			return s;
		}
		return s.substring( s.length() - len, s.length() );
	}
}
